from pathlib import Path
import matplotlib.pyplot as plt
from pprint import pp

if 0:
    from ase.db import connect
    dbs = []
    for p in (Path.home() / 'cmr/docs/').glob('*/*.db'):
        dbs.append((p.name[:-3], p.stat().st_size, len(connect(p))))
    pp(dbs)

dbs = [('low_symmetry_perovskites', 4262912, 1984),
       ('abse3', 13000704, 5976),
       ('oqmd123', 21827584, 9413),
       ('bondmin', 2228224, 1190),
       ('dssc', 119530496, 12096),
       ('agau309', 2912256, 310),
       # ('lowdim', 1072135168, 167767),
       ('solids', 1561600, 1146),
       ('molecules', 4115456, 2841),
       ('oqmd12', 12394496, 4337),
       ('mp_gllbsc', 4542464, 2398),
       ('c2db', 4837376, 4000),
       ('organometal', 618496, 240),
       ('absorption_perovskites', 4213760, 79),
       ('funct_perovskites', 294912, 94),
       ('g2', 69138432, 8895),
       ('adsorption', 3719168, 200),
       ('surfaces', 466944, 25),
       ('compression', 85362688, 20316),
       ('catapp', 6207488, 3269),
       ('fcc111', 5214208, 1922),
       ('htgw', 23994368, 556),
       ('a2bcx4', 58380288, 1368),
       ('abx2', 141578240, 123),
       ('pv_pec_oqmd', 9666560, 7240),
       ('solar', 32364544, 5366),
       ('cubic_perovskites', 47226880, 19369),
       ('gbrv', 37895168, 10590),
       ('abs3', 303951872, 4079)]

dbs.sort(key=lambda x: x[2])
N = 7
dbs1 = [('other', 0, sum(n for _, _, n in dbs[:N]))] + dbs[N:]
dbs2 = dbs[:N]
pp(dbs2)
print('``, ``'.join(reversed([name for name, _, _ in dbs2])),
      dbs1)


def make_pies():
    fig, ax1 = plt.subplots()#2, 1)
    labels, size, configs = zip(*dbs1)
    ax1.pie(configs, labels=labels,
            explode=[0.6] + [0] * (len(dbs1) - 1))
    ax1.set_title(f'Total: {sum(configs)} rows')
    #labels, size, configs = zip(*dbs2)
    #ax2.pie(configs, labels=labels)
    #ax2.set_title(f'Other: {sum(configs)}')
    plt.tight_layout()
    # plt.show()
    plt.savefig('slides/pie.svg')


if __name__ == '__main__':
    make_pies()
