import sys
sys.path.append('.')
project = 'ase-optimade'
copyright = '2022, Jens Jørgen Mortensen'
author = 'Jens Jørgen Mortensen'
extensions = ['sphinx.ext.extlinks',
              'sphinx.ext.intersphinx',
              'make_slides']
extlinks = {'doi': ('https://doi.org/%s', 'doi: %s')}
templates_path = ['_templates']
exclude_patterns = ['_build', 'README.rst', 'venv']
html_theme = 'bizstyle'
html_theme_options = {'nosidebar': True,
                      'navigation_with_keys': True}
# default_role = 'git'
