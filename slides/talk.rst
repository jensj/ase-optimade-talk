.. role:: strikethrough
.. role:: red
.. role:: green
.. highlight:: bash

.. _ASE: https://wiki.fysik.dtu.dk/ase/
.. _reStructuredText: https://docutils.sourceforge.io/rst.html

.. _all slides:

Make the ase.db module speak OPTIMADE (status update)
=====================================================

**Open Databases Integration for Materials Design** (OPTIMADE_)

*May 30, 2022 - June 3, 2022*

*CECAM-HQ-EPFL, Lausanne, Switzerland*

::

 Jens Jørgen Mortensen
 Department of Physics
 Technical University of Denmark

1) quick introduction to the ``ase.db`` module in the atomic simulation
   environment (ASE_) Python library

2) our computational materials repository (CMR_) databases

3) how to make ``ase.db`` databases speak the OPTIMADE protocol

4) questions


.. _slides: https://jensj.gitlab.io/ase-optimade-talk/
.. _source: https://gitlab.com/jensj/ase-optimade.talk
.. _OPTIMADE: https://www.optimade.org/
.. _CMR: https://cmr.fysik.dtu.dk/


Simple ase.db example
=====================

https://wiki.fysik.dtu.dk/ase/ase/db/db.html

.. code-block:: python

 >>> from ase.db import connect
 >>> from ase import Atoms
 >>> from ase.calculators.emt import EMT
 >>> db = connect('abc.db')
 >>> h = Atoms('H')
 >>> db.write(h)
 >>> h.calc = EMT()
 >>> f = h.get_forces()
 >>> something = np.array([1.0, 2.0, 3.0])
 >>> db.write(h, abc=42, data={'thing': something})
 >>> for row in db.select(abc=42):
 ...     print(row.data.thing)
 ...     print(row.toatoms().get_forces())
 ...
 [1. 2. 3.]
 [[0. 0. 0.]]


Back-ends
=========

There are currently five back-ends:

SQLite3_:
    Self-contained, server-less, zero-configuration database.  Lives in a file
    with a ``.db`` extension.
PostgreSQL_:
    Server based database.
MySQL_:
    Server based database.
MariaDB_:
    Server based database.
JSON_:
    Simple human-readable text file with a ``.json`` extension.

The JSON and SQLite3 back-ends work "out of the box".  Nice for easy sharing
of data (a single file).

PostgreSQL, MySQL and MariaDB back-ends require a server (gives better query
performance).


.. _JSON: http://www.json.org/
.. _SQLite3: https://www.sqlite.org/index.html
.. _PostgreSQL: https://www.postgresql.org/
.. _MySQL: https://www.mysql.com/
.. _MariaDB: https://mariadb.org/


Command-line tool
=================

There is a command-line tool called ``ase db`` that can be
used to query and manipulate databases:

.. code-block:: bash

    $ ase db abc.db --columns +abc
    id|age|formula|calculator|energy| fmax|pbc|abc
     1|15m|H      |          |      |     |FFF|
     2|15m|H      |emt       | 3.210|0.000|FFF| 42
    Rows: 2
    $ ase db abc.db "abc=42" --insert-into abc-42.db

Query specification:

  https://wiki.fysik.dtu.dk/ase/ase/db/db.html#querying


Web interface
=============

You can use your web-browser to look at and query databases like this:

.. code-block:: bash

    $ ase db abc.db --open-web-browser
    $ firefox http://0.0.0.0:5000/


Computational Materials Project
===============================

https://cmr.fysik.dtu.dk/ and https://cmrdb.fysik.dtu.dk/

.. image:: pie.svg
    :width: 75 %

``lowdim``: 167767 rows.

Other (1071 rows): ``agau309``, ``organometal``, ``adsorption``, ``abx2``,
``funct_perovskites``, ``absorption_perovskites`` and ``surfaces``.


The CMR web-pages
=================

The web-page for the projects under the CMR umbrella (like C2DB) use the
``ase.db.app`` code:

* We use PostgreSQL_ for the database
* we use uwsgi_ to run the Flask_ app
* Each project has customized query-pages and row-pages
  (via Python code and Jinja2_ templates)

.. _Flask: https://palletsprojects.com/p/flask/
.. _WSGI: https://www.python.org/dev/peps/pep-3333/
.. _jinja2: https://jinja2docs.readthedocs.io/en/stable/
.. _uwsgi: https://uwsgi-docs.readthedocs.io/en/latest/
.. _PostgreSQL: https://www.postgresql.org/


What's in the CMR Git-repository?
=================================

https://gitlab.com/camd/cmr

* code for running the https://cmrdb.fysik.dtu.dk/ server.
* code and reStructuredText_ files for the https://cmr.fysik.dtu.dk/ web-page
* file layout for each project::

   ├── cmr
   │   ├── proj1
   │   │   ├── custom.py
   │   │   ├── search.html
   │   │   ├── layout.html
   │   │   ├── things.py
   │   │   ├── stuff.py
   │   │   ├── workflow.py
   │   │   ├── test_1.py
   │   │   └── test_2.py
   ├── docs
   │   ├── proj1
   │   │   ├── proj1.rst
   │   │   ├── (proj1.db)
   │   │   ├── example1.py
   │   │   └── example2.py


ASE+OPTIMADE server
===================

Possible strategies:

* Convert OPTIMADE filter-string to ``ase.db`` syntax (ASE's query language
  is not powerful enough to implement what OPTIMADE needs)

* Convert OPTIMADE filter-string directly to SQL matching ``ase.db``'s
  SQL schema (big task, maybe later)

* Convert ``ase.db`` database to in-memory Python representation that can be
  scanned by a filter-function -- the filter-function should take as input a row
  and a tree-representation of the query (simple to implement)


.. admonition:: Proof-of-concept ASE+OPTIMADE server

  https://github.com/Materials-Consortia/optimade-python-tools/pull/856


Questions
=========

* Feedback?
* ``_ase`` prefix?  (``_ase_abc=42``)
* How to best serve several unrelated databases?
* Your questions?


What's in an ase.db database
============================

ASE has its own database that can be used for storing and retrieving atoms and
associated data in a compact and convenient way.

https://wiki.fysik.dtu.dk/ase/ase/db/db.html

Every row in the database contains:

Taxonomy:
    * ``Atoms`` object (positions, atomic numbers, ...)
    * Calculator name and parameters
    * Energy, forces, stress tensor, dipole moment, magnetic moments

Folksonomy:
    * Key-value pairs: ``str``, ``float``, ``int``, ``bool``
      (used for queries)

Additional things:
    * Extra data: ``dict`` (band structure, dos, ...)


Python API
==========

* ``ase.db.connect``:  Create connection to database
* ``ase.db.Database.write``:  Write atoms, key-value pairs and data to database
* ``ase.db.Database.select``:  Query database
* ...
